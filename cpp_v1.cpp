#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

double **matrix_alloc(int N);
void matrix_free(int N, double **);
void matrix_fill(int N, double **A);
void matrix_print(int N, double **A);
void matrix_mult(int N, double **A, double **B, double **C);

int main(int argc, char *argv[])
{
	double **A, **B, **C;
	int N;
	clock_t t1, t2;

	if (argc < 2)
		return 1;

	N = atoi(argv[1]);
	if (N < 1)
		return 1;

	A = matrix_alloc(N);
	B = matrix_alloc(N);
	C = matrix_alloc(N);

	matrix_fill(N, A);
	matrix_fill(N, B);

	t1 = clock();
	matrix_mult(N, A, B, C);
	t2 = clock();
	cout << (double)(t2 - t1) / CLOCKS_PER_SEC << "\n";
	cout << C[N/2][N/2] << "\n";

	matrix_free(N, A);
	matrix_free(N, B);
	matrix_free(N, C);

	return 0;
}

double **matrix_alloc(int N)
{
	int i;
	double **result;

	result = new double*[N];

	for (i = 0; i < N; ++i)
		result[i] = new double[N];

	return result;
}

void matrix_free(int N, double **A)
{
	int i;

	for (i = 0; i < N; ++i)
		delete[] A[i];

	delete[] A;
}

void matrix_fill(int N, double **A)
{
	int i, j;
	double tmp;

	tmp = 1. / N / N;

	for (i = 0; i < N; ++i)
		for (j = 0; j < N; ++j)
			A[i][j] = tmp * (i - j) * (i + j);
}

void matrix_print(int N, double **A)
{
	int i, j;

	for (i = 0; i < N; ++i) {
		for (j = 0; j < N; ++j) {
			printf("% 8.6f ", A[i][j]);
		}
		puts("");
	}
}

void matrix_mult(int N, double **A, double **B, double **C)
{
	int i, j, k;
	double **T = matrix_alloc(N);

	for (i = 0; i < N; ++i)
		for (j = 0; j < N; ++j)
			T[i][j] = B[j][i];

	for (i = 0; i < N; ++i)
		for (j = 0; j < N; ++j)
			for (k = 0; k < N; ++k)
				C[i][j] += A[i][k] * T[j][k];

	matrix_free(N, (double **)T);
}
