#include <iostream>
#include <vector>
#include <ctime>

using namespace std;

void matrix_fill(vector<vector<double>> &A);
void matrix_mult(vector<vector<double>> &A,
                 vector<vector<double>> &B,
                 vector<vector<double>> &C);

int main(int argc, char *argv[])
{
	if (argc < 2)
		return 1;

	int N = stoi(argv[1]);

	vector<vector<double>> A(N, vector<double>(N));
	vector<vector<double>> B(N, vector<double>(N));
	vector<vector<double>> C(N, vector<double>(N, 0.0));

	matrix_fill(A);
	matrix_fill(B);

	clock_t t1 = clock();
	matrix_mult(A, B, C);
	clock_t t2 = clock();

	cout << (double)(t2 - t1) / CLOCKS_PER_SEC << "\n";
	cout << C[N/2][N/2] << "\n";
}

void matrix_fill(vector<vector<double>> &A)
{
	int N = A.size();
	double tmp;

	tmp = 1. / N / N;

	for (int i = 0; i < N; ++i)
		for (int j = 0; j < N; ++j)
			A[i][j] = tmp * (i - j) * (i + j);
}

void matrix_mult(vector<vector<double>> &A,
                 vector<vector<double>> &B,
                 vector<vector<double>> &C)
{
	int N = A.size();
	vector<vector<double>> T(N, vector<double>(N));

	for (int i = 0; i < N; ++i)
		for (int j = 0; j < N; ++j)
			T[i][j] = B[j][i];

	for (int i = 0; i < N; ++i)
		for (int j = 0; j < N; ++j)
			for (int k = 0; k < N; ++k)
				C[i][j] += A[i][k] * T[j][k];
}
