#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void matrix_fill(int N, double *A);
void matrix_print(int N, double *A);
void matrix_mult(int N, double *restrict A, double *restrict B, double *restrict C);

int main(int argc, char *argv[])
{
	double *A, *B, *C;
	int N;
	clock_t t1, t2;

	if (argc < 2)
		return 1;

	N = atoi(argv[1]);
	if (N < 1)
		return 1;

	A = (double *)malloc(N * N * sizeof(double));
	B = (double *)malloc(N * N * sizeof(double));
	C = (double *)calloc(N * N, sizeof(double));

	matrix_fill(N, A);
	matrix_fill(N, B);

	t1 = clock();
	matrix_mult(N, A, B, C);
	t2 = clock();
	printf("%f\n", (double)(t2 - t1) / CLOCKS_PER_SEC);
	printf("%f\n", *(C + N/2 * N + N/2));

	free(A);
	free(B);
	free(C);

	return 0;
}

void matrix_fill(int N, double *A)
{
	int i, j;
	double tmp;

	tmp = 1. / N / N;

	for (i = 0; i < N; ++i)
		for (j = 0; j < N; ++j)
			*(A + N * i + j) = tmp * (i - j) * (i + j);
}

void matrix_print(int N, double *A)
{
	int i;

	for (i = 0; i < N * N; ++i) {
		printf("% 8.6f ", *A++);
		if ((i + 1) % N  == 0)
			printf("\n");
	}
}

void matrix_mult(int N, double *restrict A, double *restrict B, double *restrict C)
{
	int i, j, k;
	double *restrict T = (double *)malloc(N * N * sizeof(double));

	for (i = 0; i < N; ++i)
		for (j = 0; j < N; ++j)
			*(T + i + N*j) = *(B + N*i + j);

	for (i = 0; i < N; ++i)
		for (j = 0; j < N; ++j)
			for (k = 0; k < N; ++k)
				*(C + N*i + j) += *(A + N*i + k) * *(T + N*j + k);
	free(T);
}
