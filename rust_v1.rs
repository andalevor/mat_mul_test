#![feature(core_intrinsics)] //nightly needed

use std::intrinsics::{fadd_fast, fmul_fast};
use std::env;
use std::time::Instant;

fn main() {
    let args: Vec<String> = env::args().collect();

    let n: usize = args[1].trim().parse()
        .expect("Please type a number!");

    //arrays are immutable so I'll use Vec
    let mut a = vec![0.0 as f64; n * n];
    let mut b = vec![0.0 as f64; n * n];
    let mut c = vec![0.0 as f64; n * n];

    matrix_fill(n, &mut a);
    matrix_fill(n, &mut b);

    let t1 = Instant::now();
    matrix_mult(n, &a, &b, &mut c);
    let t2 = Instant::now();

    println!("{:?}\n{}", t2.duration_since(t1), c[n*(n/2) + n/2]);
}

fn matrix_mult(n: usize, a: &Vec<f64>, b: &Vec<f64>, c: &mut Vec<f64>) {
    let mut t = vec![0.0 as f64; n * n];

    unsafe {
        for i in 0..n {
    	    for j in 0..n {
                *t.get_unchecked_mut(i*n + j) = *b.get_unchecked(j*n + i);
            }
	}

        for i in 0..n {
	    for j in 0..n {
	       for k in 0..n {
	           *c.get_unchecked_mut(i*n + j) =
	               fadd_fast(*c.get_unchecked(i*n + j),
		                 fmul_fast(*a.get_unchecked(i*n + k), *t.get_unchecked(j*n + k)));
	       }
	    }
        }
    }
}

fn matrix_fill(n: usize, a: &mut Vec<f64>) {
    let tmp: f64 = 1.0 as f64 / n as f64 / n as f64;
    for i in 0..n {
        for j in 0..n {
	    a[i*n + j] = tmp * (i as i32 - j as i32) as f64 * (i + j) as f64;
	}
    }
}

fn matrix_print(n: usize, a: &Vec<f64>) {
    for i in 0..n {
        for j in 0..n {
	    print!("{:+07} ", a[i*n + j]);
	}
	println!();
    }
}